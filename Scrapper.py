import cv2
import os
import numpy as np
import urllib.request
import time



start_time = time.time()

for i in range(1, 51):
    try:
        request = urllib.request.Request(
            'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/' + '{:03d}'.format(i) + '.png')
        response = urllib.request.urlopen(request)
        binary_str = response.read()
        byte_array = bytearray(binary_str)
        image = np.asarray(byte_array, dtype="uint8")
        image = cv2.imdecode(image, cv2.IMREAD_UNCHANGED)
        os.chdir('C:\\Users\\Dell\\Desktop\\Clg Projects\\OS\\DownloadedFiles')
      
        if(os.path.isdir('images')==False):
            os.mkdir('images')

        cv2.imwrite("images/" + '{:04d}'.format(i) + ".png", image)
        print("Saved " + '{:04d}'.format(i) + ".png")
        
        
    except Exception as e:
        print("Error Occured for Pokemon " + '{:04d}'.format(i))
        print(str(e))
        
end_time = time.time()
print("Done")
print("Time Taken = ", end_time - start_time, "sec")




# command python -u "c:\Users\Dell\Desktop\Clg Projects\OS\pythonFiles\Scrapper.py"