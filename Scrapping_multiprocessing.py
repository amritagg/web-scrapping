# Submitted by: 
# Ayussh Vashishth (2K19/EC/039) && Amrit (2K19/EC/014)


from multiprocessing import Pool
import cv2,os,numpy as np,urllib.request,time, matplotlib.pyplot as plt

base_url = 'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/'
all_urls = []

def generate_urls():
    for i in range(1,51):
        all_urls.append(base_url + '{:03d}'.format(i) + '.png')

def scrape(url):
   
    request = urllib.request.Request(url)
    response = urllib.request.urlopen(request)
    binary_str = response.read()
    byte_array = bytearray(binary_str)
    numpy_array = np.asarray(byte_array, dtype="uint8")
    image = cv2.imdecode(numpy_array, cv2.IMREAD_UNCHANGED)
    cv2.imwrite("images_processing/0" + url[len(base_url):], image)
    # print("saved imgage!")



if __name__ == '__main__':
    generate_urls()
    os.chdir('C:\\Users\\Dell\\Desktop\\Clg Projects\\OS\\DownloadedFiles') 
    if(os.path.isdir('images_processing')==False):
        os.mkdir('images_processing')
    timelist=[]

    yplot = [0,0,0,0,0,0,0,0]
    xplot = [1,2,3,4,5,6,7,8]

    for n in range(1,9):
        start_time = time.time()
    
        p = Pool(n)
        print("For "+ str(n) + " processors:")

        p.map(scrape, all_urls)
        # p.close()
        # p.join()

        end_time = time.time()
        yplot[n-1] = end_time-start_time
        print("time taken is = "+ str(end_time-start_time))

    plt.plot(xplot,yplot)
    plt.xlabel("Number of processers -> ")
    plt.ylabel("Time taken -> ")
    plt.title("Time vs No. of cores")
    plt.show()


# command python -u "c:\Users\Dell\Desktop\Clg Projects\OS\pythonFiles\Scrapping_multiprocessing.py"